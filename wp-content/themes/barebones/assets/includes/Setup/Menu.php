<?php

namespace Inc\Setup;

/**
 * Menu
 */
class Menu {
    /**
     * register default hooks and actions for WordPress
     * @return
     */
    public function register() {
        add_action('after_setup_theme', array($this, 'menus'));
        add_filter('wp_nav_menu_args', array($this, 'ondigitals_nav_menu_args'));
    }

    public function menus() {
        /*
        Register all your menus here
         */
        register_nav_menus([
            'header' => 'Header',
            'footer' => 'Footer',
            'off-canvas' => 'Canvas Menu',
        ]);
    }

    function ondigitals_nav_menu_args($args) {
        /**
         * Nav menu args
         *
         * @param array $args
         * @return void
         */
        $args['container'] = false;
        $args['container_class'] = false;
        $args['menu_id'] = false;
        $args['items_wrap'] = '<ul class="%2$s">%3$s</ul>';

        return $args;
    }

}
