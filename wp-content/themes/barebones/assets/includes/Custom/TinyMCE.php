<?php

namespace Inc\Custom;

/**
 * TinyMCE
 *
 * @param array $buttons
 * @return void
 */

class TinyMCE {
    /**
     * register default hooks and actions for WordPress
     * @return
     */
    public function register() {
        add_filter('mce_buttons_2', array($this, 'ondigitals_mce_buttons_2'));
        add_filter('tiny_mce_before_init', array($this, 'ondigitals_tiny_mce_before_init'));
    }

    public function ondigitals_mce_buttons_2($buttons) {
        array_unshift($buttons, 'styleselect');
        $buttons[] = 'hr';

        return $buttons;
    }
    /**
     * TinyMCE styling
     *
     * @param array $settings
     * @return void
     */

    public function ondigitals_tiny_mce_before_init($settings) {
        $style_formats = [
            // [
            //     'title'    => '',
            //     'selector' => '',
            //     'classes'  => ''
            // ],
            // [
            //     'title' => 'Buttons',
            //     'items' => [
            //         [
            //             'title'    => 'Primary',
            //             'selector' => 'a',
            //             'classes'  => 'btn btn--primary'
            //         ],
            //         [
            //             'title'    => 'Secondary',
            //             'selector' => 'a',
            //             'classes'  => 'btn btn--secondary'
            //         ]
            //     ]
            // ]
        ];

        $settings['style_formats'] = json_encode($style_formats);
        $settings['style_formats_merge'] = true;

        return $settings;
    }
}
