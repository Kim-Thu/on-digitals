<?php
/**
 * Callbacks for Settings API
 *
 * @package ondigitals
 */

namespace Inc\Api\Callbacks;

/**
 * Settings API Callbacks Class
 */
class SettingsCallback {
    public function admin_index() {
        return require_once get_template_directory() . '/inc/admin/index.php';
    }

    public function admin_faq() {
        echo '<div class="wrap"><h1>FAQ Page</h1></div>';
    }

    public function ondigitals_options_group($input) {
        return $input;
    }

    public function ondigitals_admin_index() {
        echo 'Customize this Theme Settings section and add description and instructions';
    }

    public function google_map() {
        $google_map = esc_attr(get_option('google_map'));
        echo '<input id="google_map" type="text" class="regular-text" name="google_map" value="' . $google_map . '" placeholder="Google Map" />';
    }
}