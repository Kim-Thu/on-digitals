<?php
/**
 * Template part for displaying a custom Admin area
 *
 * @link https://developer.wordpress.org/reference/functions/add_menu_page/
 *
 * @package ondigitals
 */

?>

<div class="wrap">
	<h1><?php __('Settings Page', 'ondigitals')?></h1>
	<?php settings_errors();?>

	<form method="post" action="options.php">
		<?php settings_fields('ondigitals_options_group');?>
		<?php do_settings_sections('ondigitals');?>
		<?php submit_button();?>
	</form>
</div>
