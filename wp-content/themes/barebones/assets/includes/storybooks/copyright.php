 <p class="footer__copyright">&copy; <?php echo date('Y'); ?> <a href="<?php echo get_home_url(); ?>" title="<?php echo get_bloginfo('name'); ?>"><?php echo get_bloginfo('name'); ?></a>
    <?php printf(
    '<span %s>%s</span>',
    is_customize_preview() ? 'id="ondigitals-footer-copy-control"' : '',
    esc_html(Inc\Api\Customizer::text('ondigitals_footer_copy_text')));?>
</p>