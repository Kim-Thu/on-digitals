<nav class="footer__navigation">
    <?php wp_nav_menu(['theme_location' => 'footer', 'menu_class' => 'nav nav--footer']);?>
</nav>