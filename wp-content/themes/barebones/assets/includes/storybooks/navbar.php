<a href="#" class="nav-burger js-menu-toggle">
  <span class="nav-burger__line"></span>
  <span class="nav-burger__line"></span>
  <span class="nav-burger__line"></span>
</a>
<nav role="navigation" class="header__navbar">
<?php
if (has_nav_menu('header')):
    wp_nav_menu(
        array(
            'theme_location' => 'header',
            'menu_id' => 'header-menu',
            'menu_class' => 'navbar',
            'walker' => new Inc\Custom\WalkerNav(),
        )
    );
endif;
?>
</nav>