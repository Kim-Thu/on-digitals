<?php

namespace Inc\Custom;

/**
 * Custom
 * use it to write your custom functions.
 */
class ShortCodes {
    /**
     * register default hooks and actions for WordPress
     * @return
     */
    public function register() {

        add_shortcode('button', array($this, 'ondigitals_button_shortcode'));
    }

    /**
     * Button Shortcode
     *
     * @param array $atts
     * @param string $content
     * @return void
     */

    public function ondigitals_button_shortcode($atts, $content = null) {
        $atts['class'] = isset($atts['class']) ? $atts['class'] : 'btn';
        return '<a class="' . $atts['class'] . '" href="' . $atts['link'] . '">' . $content . '</a>';
    }
}
