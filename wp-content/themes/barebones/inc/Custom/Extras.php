<?php

namespace Inc\Custom;

/**
 * Extras.
 */
class Extras {
    /**
     * register default hooks and actions for WordPress
     * @return
     */
    public function register() {
        add_filter('body_class', array($this, 'body_class'));
        add_action('admin_menu', array($this, 'front_page_on_pages_menu'));
    }

    public function body_class($classes) {

        // Adds a class of group-blog to blogs with more than 1 published author.
        if (is_multi_author()) {
            $classes[] = 'group-blog';
        }
        // Adds a class of hfeed to non-singular pages.
        if (!is_singular()) {
            $classes[] = 'hfeed';
        }

        return $classes;
    }

    public function get_post_thumbnail_url($size = 'full', $post_id = false, $icon = false) {
        if (!$post_id) {
            $post_id = get_the_ID();
        }

        $thumb_url_array = wp_get_attachment_image_src(
            get_post_thumbnail_id($post_id), $size, $icon
        );
        return $thumb_url_array[0];
    }

    public function front_page_on_pages_menu() {
        global $submenu;
        if (get_option('page_on_front')) {
            $submenu['edit.php?post_type=page'][501] = array(
                __('Front Page', 'ondigitals'),
                'manage_options',
                get_edit_post_link(get_option('page_on_front')),
            );
        }
    }

}
