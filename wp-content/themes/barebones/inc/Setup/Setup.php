<?php

namespace Inc\Setup;

class Setup {
    /**
     * register default hooks and actions for WordPress
     * @return
     */
    public function register() {
        add_action('after_setup_theme', array($this, 'remover'));
        add_action('after_setup_theme', array($this, 'setup'));
        add_action('after_setup_theme', array($this, 'content_width'), 0);
        /**
         * Hide admin bar
         */

        add_filter('show_admin_bar', '__return_false');

    }

    public function remover() {
        /**
         * Remove junk
         */
        remove_action('wp_head', 'rsd_link');
        remove_action('wp_head', 'wlwmanifest_link');
        remove_action('wp_head', 'wp_generator');
        remove_action('wp_head', 'start_post_rel_link');
        remove_action('wp_head', 'index_rel_link');
        remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');
        remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
        remove_action('wp_head', 'print_emoji_detection_script', 7);
        remove_action('wp_print_styles', 'print_emoji_styles');
    }

    public function setup() {
        /*
         * You can activate this if you're planning to build a multilingual theme
         */
        // load_theme_textdomain('ondigitals', get_template_directory() . '/languages');

        /*
         * Default Theme Support options better have
         */
        if (function_exists('add_theme_support')) {
            add_theme_support('automatic-feed-links');
            add_theme_support('title-tag');
            add_theme_support('post-thumbnails');
            add_theme_support('customize-selective-refresh-widgets');
            add_theme_support('menus');
            add_theme_support('custom-logo');
            // add_theme_support('custom-header', array('uploads' => true));

            /**
             * Add woocommerce support and woocommerce override
             */
            // add_theme_support('woocommerce');

            add_theme_support('html5', array(
                'search-form',
                'comment-form',
                'comment-list',
                'gallery',
                'caption',
            ));

            // add_theme_support('custom-background', apply_filters('inc_custom_background_args', array(
            //     'default-color' => 'ffffff',
            //     'default-image' => '',
            // )));

            /**
             * Activate Post formats if you need
             */
            add_theme_support('post-formats', array(
                'aside',
                'gallery',
                'link',
                'image',
                'quote',
                'status',
                'video',
                'audio',
                'chat',
            ));
        }
    }

    /*
    Define a max content width to allow WordPress to properly resize your images
     */
    public function content_width() {
        $GLOBALS['content_width'] = apply_filters('content_width', 1440);
    }

}
