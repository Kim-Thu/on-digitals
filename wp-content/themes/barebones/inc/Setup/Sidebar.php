<?php

namespace Inc\Setup;

/**
 * Sidebar.
 */
class Sidebar {
    /**
     * register default hooks and actions for WordPress
     * @return
     */
    public function register() {
        add_action('widgets_init', array($this, 'theme_widgets_init'));
    }

    /*
    Define the sidebar
     */
    public function theme_widgets_init() {
        if (function_exists('register_sidebar')) {
            register_sidebar(array(
                'name' => esc_html__('Sidebar', 'ondigitals'),
                'id' => 'awps-sidebar',
                'description' => esc_html__('Default sidebar to add all your widgets.', 'ondigitals'),
                'before_widget' => '<section id="%1$s" class="widget %2$s p-2">',
                'after_widget' => '</section>',
                'before_title' => '<h2 class="widget-title">',
                'after_title' => '</h2>',
            ));
        }
    }
}