<?php

namespace Inc\Setup;

/**
 * Enqueue.
 */
class Enqueue {
    /**
     * register default hooks and actions for WordPress
     * @return
     */
    public function register() {
        add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts'));
        add_action('script_loader_tag', array($this, 'defer_scripts'), 10, 3);
        add_action('wp_head', array($this, 'add_gtag_to_head'));
        add_action('wp_footer', array($this, 'deregister_scripts'));
        add_action('wp_print_styles', array($this, 'deregister_styles'), 100);
    }

    /**
     * Notice the mix() function in wp_enqueue_...
     * It provides the path to a versioned asset by Laravel Mix using querystring-based
     * cache-busting (This means, the file name won't change, but the md5. Look here for
     * more information: https://github.com/JeffreyWay/laravel-mix/issues/920 )
     */
    public function enqueue_scripts() {
        // Deregister the built-in version of jQuery from WordPress
        if (!is_customize_preview()) {
            wp_deregister_script('jquery');
        }
// wp_enqueue_style( 'fonts', '//fonts.googleapis.com/css?family=Font+Family' );
        // wp_enqueue_style( 'icons', '//use.fontawesome.com/releases/v5.0.10/css/all.css' );
        wp_enqueue_style('admin-styles', get_stylesheet_directory_uri() . '/css/admin.css?' . filemtime(get_stylesheet_directory() . '/css/admin.css'));
        wp_enqueue_script('admin-scripts', get_stylesheet_directory_uri() . '/js/admin.js?' . filemtime(get_stylesheet_directory() . '/js/admin.js'), [], null, true);
        wp_enqueue_style('styles', get_stylesheet_directory_uri() . '/css/style.css?' . filemtime(get_stylesheet_directory() . '/style.css'));
        wp_enqueue_script('scripts', get_stylesheet_directory_uri() . '/js/bundle.js?' . filemtime(get_stylesheet_directory() . '/js/bundle.js'), [], null, true);
        wp_enqueue_script('comment-reply');

    }

    /**
     * Add async and defer attributes to enqueued scripts
     *
     * @param string $tag
     * @param string $handle
     * @param string $src
     * @return void
     */

    public function defer_scripts($tag, $handle, $src) {

        // The handles of the enqueued scripts we want to defer
        $defer_scripts = [
            'SCRIPT_ID',
        ];

        // Find scripts in array and defer
        if (in_array($handle, $defer_scripts)) {
            return '<script type="text/javascript" src="' . $src . '" defer="defer"></script>' . "\n";
        }

        return $tag;
    }

    public function deregister_scripts() {
        wp_deregister_script('wp-embed');
    }

    public function deregister_styles() {
        wp_dequeue_style('wp-block-library');
    }

    public function add_gtag_to_head() {
        // Check is staging environment
        if (strpos(get_bloginfo('url'), '.test') !== false) {
            return;
        }

        // Google Analytics
        $tracking_code = 'UA-*********-1';

        ?>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $tracking_code; ?>"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', '<?php echo $tracking_code; ?>');
        </script>
    <?php }
}
