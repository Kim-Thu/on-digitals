<?php
/**
 * Theme Customizer
 *
 * @package ondigitals
 */

namespace Inc\Api;

use Inc\Api\Gutenbergs\Gutenberg;

/**
 * Customizer class
 */
class Gutenbergs {
    /**
     * register default hooks and actions for WordPress
     * @return
     */
    public function register() {
        add_action('wp_head', array($this, 'output'));

        add_action('customize_register', array($this, 'setup'));
    }

    /**
     * Store all the classes inside an array
     * @return array Full list of classes
     */
    public function get_classes() {
        return [
            Gutenberg::class,
        ];
    }

    /**
     * Add postMessage support for site title and description for the Theme Customizer.
     *
     * @param WP_Customize_Manager $wp_customize Theme Customizer object.
     */
    public function setup($wp_customize) {
        foreach ($this->get_classes() as $class) {
            $service = new $class;
            if (method_exists($class, 'register')) {
                $service->register($wp_customize);
            }
        }
    }
}
