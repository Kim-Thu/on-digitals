const mix = require( 'laravel-mix' );
require( '@tinypixelco/laravel-mix-wp-blocks' );
let sassGlobImporter = require( 'node-sass-glob-importer' );

const config = {
	src: 'assets',
	dist: './',
};
const source = {
	js: [ `${ config.src }/scripts/index.js` ],
	css: [ `${ config.src }/styles/style.scss` ],
	inc: [ `${ config.src }/includes` ],
	image: [ `${ config.src }/images` ],
};

mix.autoload( {
	jquery: [ '$', 'window.$', 'window.jQuery', 'window.jquery', 'jQuery' ],
} );

mix.webpackConfig( {
	stats: {
		children: false,
	},
} );

mix.js( `${ source.js }`, `js/bundle.js` )
	.js( `${ config.src }/scripts/admin.js`, `js/admin.js` )
	.sass( `${ source.css }`, `css/style.css`, {
		sassOptions: {
			importer: sassGlobImporter(),
		},
	} )
	.sass( `${ config.src }/styles/admin/admin.scss`, `css/admin.css` )
	.options( {
		postCss: [
			require( 'postcss-preset-env' ),
			require( 'postcss-cssnext' ),
		],
	} );

mix.copyDirectory( `${ source.inc }`, `inc` );
mix.copyDirectory( `${ source.image }`, `images` );

if ( mix.inProduction ) {
	mix.options( {
		postCss: [
			require( 'cssnano' )( {
				preset: [
					'default',
					{ discardComments: { removeAll: true }, calc: false },
				],
			} ),
		],
	} );

	mix.sourceMaps( false, 'source-map' ).version();
}

mix.browserSync( {
	proxy: {
		target: 'http://ondigitals.nkt',
	},
	hot: true,
	open: true,
	watch: true,
	port: 5500,
} ).setPublicPath( `${ config.dist }` );
